<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '1995cbd4288aa60dcb7901039c0be3cc11eeae10',
        'name' => 'consyspub/ecommerce',
        'dev' => true,
    ),
    'versions' => array(
        'consyspub/ecommerce' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '1995cbd4288aa60dcb7901039c0be3cc11eeae10',
            'dev_requirement' => false,
        ),
        'doctrine/cache' => array(
            'pretty_version' => '2.1.1',
            'version' => '2.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/cache',
            'aliases' => array(),
            'reference' => '331b4d5dbaeab3827976273e9356b3b453c300ce',
            'dev_requirement' => false,
        ),
        'doctrine/dbal' => array(
            'pretty_version' => '3.3.3',
            'version' => '3.3.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/dbal',
            'aliases' => array(),
            'reference' => '82331b861727c15b1f457ef05a8729e508e7ead5',
            'dev_requirement' => false,
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => 'v0.5.3',
            'version' => '0.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
            'dev_requirement' => false,
        ),
        'doctrine/event-manager' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/event-manager',
            'aliases' => array(),
            'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
            'dev_requirement' => false,
        ),
        'guzzle/batch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/common' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/guzzle' => array(
            'pretty_version' => 'v3.9.3',
            'version' => '3.9.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzle/guzzle',
            'aliases' => array(),
            'reference' => '0645b70d953bc1c067bbc8d5bc53194706b628d9',
            'dev_requirement' => false,
        ),
        'guzzle/http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/inflection' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/iterator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/parser' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-async' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-backoff' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cookie' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-curlauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-error-response' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-history' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-md5' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-mock' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-oauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/service' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/stream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '6.5.5',
            'version' => '6.5.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '1.8.3',
            'version' => '1.8.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => '1afdd860a2566ed3c2b0b4a3de6e23434a79ec85',
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'aa5030cfa5405eccfdcb1083ce040c2cb8d253bf',
            'dev_requirement' => false,
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'fe5ea303b0887d5caefd3d431c3e61ad47037001',
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => 'a77e974a5fecb4398833b0709210e3d5e334ffb0',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '749045c69efb97c70d25d7463abba812e91f3a44',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
    ),
);

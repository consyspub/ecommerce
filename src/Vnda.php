<?php

namespace Consyspub\Ecommerce;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;

class Vnda
{
    public const URL_DEV = 'https://tataknitwear.vnda.dev/api/v2/';
    public const URL = 'https://tataknitwear.vnda.dev/api/v2/';

    protected Client $http;
    protected string $url;
    protected string $client;
    protected string $secret;
    protected ?string $accessToken = null;
    protected ?string $refreshToken = null;

    public function __construct(
        string $url,
        string $client,
        string $secret,
        string $code,
        ?string $accessToken,
        ?string $refreshToken
    ) {
        $this->client = $client;
        $this->secret = $secret;
        $this->code = $code;
        $this->accessToken = $accessToken;
        $this->refreshToken = $refreshToken;
        $this->url = $url;

        $this->http = new Client([
            'base_uri' => $this->url,
        ]);
    }

    public function fetchProducts()
    {
        $res = $this->send('products');

        return static::decodeResponse($res);
    }

    public function getProduct(int $id)
    {
        return static::decodeResponse($this->send("products/$id"));
    }

    public function convertProductToSql($product)
    {
        var_dump(
            DB::getBuilder()
                ->insert('PRODUTO')
                ->values([
                    'P.DESCRICAO' => $product['name'],
                    'N.NCM' => $product['ncm'],
                    'PW.DESCRICAO' => $product['description'],
                    'PW.DESCRICAO_REDUZIDA' => $product['description_small'],
                    'PW.PRECO_VENDA' => $product['price'],
                    'PW.PRECO_CUSTO' => $product['cost_price'],
                    'PW.PRECO_PROMOCAO' => $product['promotional_price'],
                    'PW.PROMOCAO_INICIO' => $product['start_promotion'],
                    'Date	PW.PROMOCAO_FIM' => $product['end_promotion'],
                    'PW.VALOR_IPI' => $product['ipi_value'],
                    'PW.MARCA' => $product['brand'],
                    'PW.MODELO' => $product['model'],
                    'P.PESO_LIQUIDO' => $product['weight'],
                    'P.COMPRIMENTO' => $product['length'],
                    'P.LARGURA' => $product['width'],
                    'P.ALTURA' => $product['height'],
                    'só vai nas variações (grade)' => $product['stock'],
                    ' =' > $product['category_id'],
                    'PW.DISPONIBILIDADE' => $product['available'],
                    // 'null' => $product['availability'],
                    'PW.DISPONIBILIDADE_DIAS' => $product['availability_days'],
                    'P.CODIGO' => $product['reference'],
                    'PW.DESTAQUE' => $product['hot'],
                    'PW.LANCAMENTO' => $product['release'],
                    'null' => $product['additional_button'],
                    ' =' > $product['related_categories'],
                    'Date	PW.LANCAMENTO_DATA' => $product['release_date'],
                    'Object	Metatags' => $product['metatag'],
                    'String	Tipo da metatag (Informar o valor keywords)' =>
                    $product['type'],
                    'String	Dados da metatag' => $product['content'],
                    'Number	Local da metatag (Informar p valor 1)' =>
                    $product['local'],
                    'PW.VIRTUAL' => $product['virtual_product'],
                    'PW.WS_PRODUTO_ID' => $product['id'],
                ])
                ->getSQL()
        );
    }

    public function auth()
    {

        $res = null;
        // if ($this->refresh_token) {
        $res = $this->_send(
            'https://tataknitwear.vnda.dev/api/v2/users/authorize',
            'POST',
            [
                'Authorization' => "Bearer 1Sz9vRj91Ys83kYunYnRVt6W",
                // 'password' => "",
            ],
            [
                'request_type' => 'json',
            ]
        );
        // } else {
        //     $res = $this->_send('auth', 'POST', [
        //         'code' => $this->code,
        //         'consumer_key' => $this->client,
        //         'consumer_secret' => $this->secret,
        //     ]);
        // }

        $data = static::decodeResponse($res);
        $this->accessToken = $data['access_token'];
        $this->refreshToken = $data['refresh_token'];

        return $data;
    }

    public static function decodeResponse(Response $res)
    {
        return json_decode((string) $res->getBody(), true);
    }

    ## BUSCAR LISTAGEM DE PRODUTOS
    public function listProducts(array $opts = [])
    {
        return $this->genericList('products', '', $opts);
    }

    ## BUSCAR DADOS COMPLETOS DE UM PRODUTO
    public function getCompleteProduct($id)
    {
        $res = $this->send(
            "products/$id",
            'get',
            [
                'sort' => 'newest',
            ],
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res);
    }

    ## BUSCAR DADOS COMPLETOS DE UMA VARIACAO (grade)
    public function getCompleteVariant($id)
    {

        $res = $this->send(
            "variants/$id"
        );

        return static::decodeResponse($res);
    }

    ## BUSCAR LISTAGEM DE STATUS DOS PEDIDOS
    public function listStatus($n_page)
    {
        $data = $this->auth();
        $res = $this->send(
            'orders/statuses',
            'get',
            [
                'page' => $n_page,
                'show_backoffice' => '1',
                'sort' => 'status_asc',
            ],
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res);
    }

    ## BUSCAR LISTAGEM DE METODOS DE PAGAMENTO
    public function listMetodos()
    {
        $data = $this->auth();
        $res = $this->send(
            'payments/methods/1/active',
            'get',
            [
                'sort' => 'id_desc',
            ],
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res);
    }

    ## BUSCAR LISTAGEM DE PEDIDOS
    public function listOrders()
    {
        $res = $this->send(
            'orders',
            'get',
            [
                'sort' => 'id_desc',
            ],
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res);
    }

    ## BUSCAR DADOS COMPLETOS DE UM PEDIDO
    public function getCompleteOrder($id)
    {
        $res = $this->send(
            "orders/$id/complete",
            'get',
            [
                'sort' => 'id_desc',
            ],
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res);
    }

    ## CANCELAR PEDIDO
    public function cancelOrder($id, $motive)
    {
        $data = [
            'cancelation_data'       => $motive,
        ];

        $res = $this->send("orders/$id/cancel", 'post', $data);
        return static::decodeResponse($res);
    }

    ## CONFIRMA O PEDIDO
    public function confirmOrder($id, $confirmation_data)
    {
        if ($confirmation_data['type'] == "deposito") {

            $data = [
                'banco'       => $confirmation_data['bank'],
                'data_credito' => $confirmation_data['date'],
                'conferido_por' => $confirmation_data['conferred_by'],
            ];
        }

        $res = $this->send("orders/$id/confirm", 'post', $data);
        return static::decodeResponse($res);
    }

    ## ADICIONA DADOS DE RASTREIO DO PEDIDO
    public function addOrderTrackingDetails($id, $package_code, $tracking_information)
    {
        $res = $this->send("orders/$id/packages/$package_code/trackings", 'post', $tracking_information);
        return static::decodeResponse($res);
    }

    ## CONFIRMA O ENVIO DO PACOTE DO PEDIDO
    public function confirmOrderPackageShip($id, $package_code)
    {
        $res = $this->send("orders/$id/packages/$package_code/ship", 'patch');
        return static::decodeResponse($res);
    }

    ## CONFIRMA O RECEBIMENTO DO PACOTE DO PEDIDO
    public function confirmOrderPackageDeliver($id, $package_code)
    {
        $res = $this->send("orders/$id/packages/$package_code/deliver", 'patch');
        return static::decodeResponse($res);
    }

    ## ATUALIZAR STATUS DE PEDIDO
    public function attStatusOrder($id, $status, $sending_date = null, $sending_code = null)
    {
        if ($status == "342") {

            $data = [
                'status_id'       => $status,
                'sending_date' => $sending_date,
                'sending_code' => $sending_code,
            ];
        } else $data = ['status_id' => $status];

        $res = $this->send("orders/$id", 'put', ['Order' => $data]);
        return static::decodeResponse($res);
    }

    ## ENVIAR DADOS DA NF TRANSMITIDA
    public function sendInvoice(int $invoiceId, int $idOrder, int $packageCode)
    {
        $invoice = DB::getBuilder()
            ->select('DATA_EMISSAO', 'CODIGO', 'SERIE', 'VALOR_NOTA')
            ->from('NOTA_FISCAL')
            ->where("NOTA_FISCAL_ID = $invoiceId")
            ->fetchAssociative();
        $invoiceXML = DB::getBuilder()
            ->select('NFE_CHAVE')
            ->from('NOTA_FISCAL_XML')
            ->where("NOTA_FISCAL_ID = $invoiceId")
            ->fetchAssociative();

        $method = 'post';
        $url = "orders/$idOrder/package/$packageCode/invoices";

        $post = [
            'issued_at' => $invoice[DATA_EMISSAO],
            'number'     => $invoice[CODIGO],
            'series'         => $invoice[SERIE],
            'key'         => $invoiceXML[NFE_CHAVE],
        ];

        $invoiceItens = cursor("SELECT GRADE_ID, CFOP FROM NOTA_FISCAL_ITEM WHERE NOTA_FISCAL_ID = '$invoiceId'");

        foreach ($invoiceItens as $it) {
            $prdId = c("SELECT WS_PRODUTO_ID FROM PRODUTO_WEBSERVICE WHERE WEBSERVICE_ID = '$_SESSION[webserviceId]' AND GRADE_ID = '$it[GRADE_ID]'");
            $varId = c("SELECT WS_VARIACAO_ID FROM PRODUTO_WEBSERVICE WHERE WEBSERVICE_ID = '$_SESSION[webserviceId]' AND GRADE_ID = '$it[GRADE_ID]'");
            if ($prdId > 0 and $varId > 0) $post['ProductCfop'][] = array(
                'product_id'    => $prdId,
                'variation_id'    => $varId,
                'cfop'            => $it[CFOP],
            );
        }

        $res = $this->send($url, $method, $post);

        return static::decodeResponse($res);
    }

    ## CANCELAMENTO DA NF TRANSMITIDA
    public function cancelInvoice(int $invoiceId, int $idOrder, int $packageCode)
    {
        $invoice = DB::getBuilder()
            ->select('CODIGO')
            ->from('NOTA_FISCAL')
            ->where("NOTA_FISCAL_ID = $invoiceId")
            ->fetchAssociative();


        $method = 'del';
        $url = "orders/$idOrder/packages/$packageCode/invoices/" . $invoice[CODIGO];

        $res = $this->send($url, $method);

        return static::decodeResponse($res);
    }

    public function listCategories(array $args = [])
    {
        $res = $this->send(
            'categories',
            'get',
            array_merge(
                [
                    'sort' => 'id_desc',
                ],
                $args
            ),
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res)['Categories'];
    }

    public function listCustomers()
    {
        $res = $this->send(
            'clients',
            'get',
            [
                'sort' => 'id_desc',
            ],
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res);
    }

    public function getCustomer(int $id)
    {
        return static::decodeResponse(
            $this->send("clients/$id", 'get')
        );
    }

    public function getOrder(int $id)
    {
        return static::decodeResponse(
            $this->send("orders/$id", 'get')
        )['Order'];
    }

    public function getClient(int $id)
    {
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function send(
        string $url,
        string $method = 'get',
        array $data = [],
        $extra = []
    ) {
        $extra = array_merge(
            [
                'request_type' => 'json',
            ],
            $extra
        );

        if ($extra['request_type'] === 'query') {
            // $data['access_token'] ??= $this->accessToken;
        }
        $url = $this->url . $url;

        return $this->_send($url, $method, $data, [
            'query' => ['access_token' => $this->accessToken],
            'request_type' => $extra['request_type'],
            // 'form_params' => $data,
        ]);
    }

    protected function genericList($url, $key = null, array $opts = [])
    {
        $res = $this->send(
            $url,
            'get',
            $opts,
            [
                'request_type' => 'query',
            ]
        );

        return $key
            ? static::decodeResponse($res)[$key]
            : static::decodeResponse($res);
    }

    public function listProductsVariants(array $opts = [])
    {
        $res = $this->send(
            'products/variants',
            'get',
            array_merge(
                [
                    'sort' => 'id_desc',
                ],
                $opts
            ),
            [
                'request_type' => 'query',
            ]
        );

        return static::decodeResponse($res)['Variants'];
    }

    public function deleteAllProductVariants(int $productId)
    {
        $variants = $this->listProductsVariants([
            'product_id' => $productId,
        ]);

        foreach ($variants as $variant) {
            $this->deleteProductVariant($variant['Variant']['id']);
        }
    }

    public function deleteProduct(int $id)
    {
        $res = $this->send("products/$id", 'delete');
        return static::decodeResponse($res);
    }

    public function deleteCategory(int $id)
    {
        $res = $this->send("categories/$id", 'delete');
        return static::decodeResponse($res);
    }

    public function deleteProductVariant(int $id)
    {
        $this->send("products/variants/$id", 'delete');
        // static::decodeResponse($res);
    }

    public function _send(
        string $url,
        string $method = 'get',
        array $data = [],
        array $extra = []
    ): Response {
        $extra = array_merge(
            [
                'headers' => [],
                'request_type' => 'json',
                'query' => [],
            ],
            $extra
        );

        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
            'query' => $extra['query'],
            $extra['request_type'] => $data,
        ];

        try {
            return $this->http->$method($url, $params);
        } catch (ClientException $e) {
            var_dump((string) $e->getResponse()->getBody());
            throw new \Exception('error');
        }
    }
}

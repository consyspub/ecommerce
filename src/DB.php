<?php

namespace Consyspub\Ecommerce;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Query\QueryBuilder;

class DB
{
    protected static Connection $conn;

    public static function init()
    {
        // TODO: get from config
        static::$conn = DriverManager::getConnection([
            'driver' => 'pdo_mysql',
            'user' => BD_USER,
            'password' => BD_PASS,
            'dbname' => BD_DATABASE,
            'host' => BD_HOST,
        ]);
    }

    public static function getBuilder(): QueryBuilder
    {
        return static::$conn->createQueryBuilder();
    }

    public static function getConnection(): Connection
    {
        return static::$conn;
    }
}

DB::init();
